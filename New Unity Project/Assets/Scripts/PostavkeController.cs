﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PostavkeController : MonoBehaviour {

    public Sprite croatianFlag, englandFlag;

    public Image flagImage;


    public Text naslov, nazivJezika, tekstGumba;


	// Use this for initialization
	void Start () {
        InicijalnaProvjera(MultiLanguageManager.LANGUAGE);	
	}

    private void InicijalnaProvjera(string str)
    {
        if (str == "HRV")
        {
            Hrvatski();
        }
        else if (str == "ENG")
        {
            Engleski();
        }
    }

    public void Provjera()
    {
        string str = MultiLanguageManager.LANGUAGE;
        if (str == "HRV")
        {
            Engleski();
        }
        else if (str == "ENG")
        {
            Hrvatski();
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }


    private void Hrvatski()
    {
        flagImage.sprite = croatianFlag;
        naslov.text = "POSTAVKE";
        nazivJezika.text = "HRVATSKI";
        tekstGumba.text = "GLAVNI IZBORNIK";
        MultiLanguageManager.LANGUAGE = "HRV";
    }


    private void Engleski()
    {
        flagImage.sprite = englandFlag;
        naslov.text = "SETTINGS";
        nazivJezika.text = "ENGLISH";
        tekstGumba.text = "MAIN MENU";
        MultiLanguageManager.LANGUAGE = "ENG";
    }      
}
