﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MusicManager : MonoBehaviour {

    public static MusicManager musicManager;
    private AudioSource audioSource;

    private void Awake()
    {
        musicManager = this;
        audioSource = GetComponent<AudioSource>();
    }        

    public void Play()
    {
        Debug.Log("Play");
        audioSource.Play();
    }

    public void Stop()
    {
        Debug.Log("Stop");
        audioSource.Stop();
    }
}
