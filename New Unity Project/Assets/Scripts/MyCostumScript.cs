﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MyCostumScript : MonoBehaviour, ITrackableEventHandler {

    private TrackableBehaviour trackableBehaviour;

    private void Start()
    {
        trackableBehaviour = GetComponent<TrackableBehaviour>();

        if (trackableBehaviour)
        {
            trackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            // Play  when target is found
            MusicManager.musicManager.Play();
        }

        else
        {
            // Stop  when target is lost
            MusicManager.musicManager.Stop();
        }
    }
}
