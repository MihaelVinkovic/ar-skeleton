﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationChange : MonoBehaviour {

    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GameObject.FindObjectOfType<Animator>();
	}
	


    public void SetIdle()
    {
        animator.SetTrigger("TriggerIdle");
    }

    public void SetWalk()
    {
        animator.SetTrigger("TriggerWalk");
    }


    public void SetAttack()
    {
        animator.SetTrigger("TriggerAttack");
    }
}
