﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Translator : MonoBehaviour {

    public Text arText, postavkeText, infoText;
    private string language="";

	// Use this for initialization
	void Start () {
        language = MultiLanguageManager.LANGUAGE;
        SetLanguage();
		
	}


    private void Update()
    {
        if(language != MultiLanguageManager.LANGUAGE)
        {
            SetLanguage();
        }
        Debug.Log(language);
        Debug.Log(": " + MultiLanguageManager.LANGUAGE);
        
    }


    public void SetLanguage()
    {
        if ("ENG" == MultiLanguageManager.LANGUAGE)
        {
            language = "ENG";
            Engleski();            
        }
        else if ("HRV" == MultiLanguageManager.LANGUAGE)
        {
            language = "HRV";
            Hrvatski();            
        }
    }


    private void Hrvatski()
    {
        arText.text = "Proširena stvarnost";
        postavkeText.text = "Postavke";
        infoText.text = "Informacije";
    }


    private void Engleski()
    {
        arText.text = "Augmented reality";
        postavkeText.text = "Settings";
        infoText.text = "Info";
    }
}
