﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadSettings()
    {
        SceneManager.LoadScene("Postavke");
    }

    public void LoadMain()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void LoadAR()
    {
        SceneManager.LoadScene("AR");
    }


    public void LoadInfo()
    {
        SceneManager.LoadScene("Info");
    }
}
