﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InfoLanguageController : MonoBehaviour {

    public Text naslov, tekstGumbaMenu, textPostavkeGumba;

	// Use this for initialization
	void Start () {
        PostaviJezik();
	}
	

    private void PostaviJezik()
    {
        if(MultiLanguageManager.LANGUAGE == "HRV")
        {
            naslov.text = "INFORMACIJE";
            tekstGumbaMenu.text = "GLAVNI IZBORNIK";
            textPostavkeGumba.text = "POSTAVKE";
        }
        else if(MultiLanguageManager.LANGUAGE == "ENG")
        {
            naslov.text = "INFO";
            tekstGumbaMenu.text = "MAIN MENU";
            textPostavkeGumba.text = "SETTINGS";
        }
        else
        {
            Debug.Log("Greška prilikom čitanja odabranog jezika !!");
        }
    }

    public void UcitajGlavniIzbornik()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void UcitajPostavke()
    {
        SceneManager.LoadScene("Postavke");
    }
}
